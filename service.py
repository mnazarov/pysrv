from twisted.internet.protocol import ServerFactory, Protocol
from twisted.internet import reactor
from twisted.python import log
import hashlib, json, sys
import Queue
from worker import Worker

queues = []

class SrvProtocol(Protocol):
  def logPrefix(self):
    return "Dispatcher"

  def logmsg(self, message):
    log.msg("[MSG] %s" % message)
  def logerr(self, message):
    log.msg("[ERR] %s" % message)

  def getClientHash(self, client):
    return hashlib.md5("%s:%s" % client).hexdigest()

  def connectionMade(self): pass
  def connectionLost(self, reason): pass
  def dataReceived(self, data):
    try:
      data = data.strip()
      if len(data) <= 0:
        self.logerr("Received empty data")
      else:
        log.msg(data)
        data = json.loads(data)
        data['clientId'] = self.getClientHash(self.transport.client)

        worker = self.factory.getWorker()
        if worker != None:
          worker.put(data)
        else:
          self.logerr("All workers are busy")
    except Exception, e:
      log.err()

class SrvFactory(ServerFactory):
  protocol = SrvProtocol
  queues, busy = [ ], [ ]
  cache = { }

  def __init__(self, queues):
    self.queues = queues
  def logPrefix(self):
    return "Dispatcher.Factory"

  def getWorker(self):
    if len(self.queues) <= 0: return None
    worker = self.queues.pop()
    self.busy.append(worker)
    return worker
  def freeWorker(self, queue):
    if len(self.busy) > 0: self.busy.remove(queue)
    self.queues.append(queue)

def main(opt):
  if opt.daemonize:
    log.startLogging(open(opt.log_file, "a"))
  else:
    log.startLogging(sys.stdout)

  factory = SrvFactory(queues)

  for i in range(opt.worker_count):
    queues.append(Queue.Queue(0))
    Worker(i, queues[i], factory).start()

  port = reactor.listenTCP(opt.port, factory, interface=opt.host)
  log.msg('Serving on %s.' % (port.getHost()))
  reactor.run()
