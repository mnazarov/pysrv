list = [
  {
    'short': '-d',
    'full': '--daemonize',
    'action': 'store_true',
    'help': 'Run service as a daemon and write output in log file. Another run in current process and write logs in standard output.',
    'default': 'true'
  },
  {
    'short': '-h',
    'full': '--host',
    'type': 'string',
    'default': 'localhost',
    'help': 'The interface to listen on. Default is localhost.'
  },
  {
    'short': '-p',
    'full': '--port',
    'type': 'int',
    'default': 10000,
    'help': 'The port to listen on. Default to a random available port.'
  },
  {
    'short': '-w',
    'full': '--worker-count',
    'type': 'int',
    'default': 8,
    'help': 'The count of workers.'
  },
  {
    'short': '-l',
    'full': '--log-file',
    'type': 'string',
    'default': 'service.log',
    'help': 'Write log to file LOG_FILE.'
  },
  {
    'full': '--pid-file',
    'type': 'string',
    'default': 'service.pid',
    'help': 'Use PID file PID_FILE on startup.'
  },
  {
    'full': '--help',
    'action': 'store_true',
    'help': 'Show this help message and exit'
  },
]

def create_list(parser):
  for item in list:
    parser.add_option(
      'short' in item and item['short'] or "",
      item['full'],
      help = item['help'],
      action = 'action' in item and item['action'] or None,
      type = 'type' in item and item['type'] or None,
      default = 'default' in item and item['default'] or ""
    )
