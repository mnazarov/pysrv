#!/usr/bin/python
import os, optparse
import options, service

parser = optparse.OptionParser("", add_help_option = 0)
options.create_list(parser)
options, args = parser.parse_args()

if options.help:
  parser.print_help()
  os._exit(0)

if options.daemonize:
  pid = os.fork()
  if pid == 0:
    service.main(options)
    os.remove(options.pid_file)
    os._exit(0)
  else:
    fpid = open(options.pid_file, 'w')
    fpid.write("%d" % pid)
    fpid.close()
else:
  service.main(options)
