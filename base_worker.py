from twisted.python import log
import threading


class BaseWorker(threading.Thread):
  def __init__(self, i, queue, factory):
    self.queue = queue
    self.number = i
    self.cache = factory.cache
    self.factory = factory
    threading.Thread.__init__(self)

  def logmsg(self, message):
    log.msg("[MSG][Worker][%d] %s" % (self.number, message))
  def logerr(self, message):
    log.msg("[ERR][Worker][%d] %s" % (self.number, message))

  def run(self):
    self.logmsg("Worker %d started" % self.number)
    while "true":
      try:
        data = self.queue.get()
        self.logmsg("Receive data: %s" % data)

        cmd = 'cmd' in data and data['cmd'] or None
        if cmd == None:
          raise Exception("cmd undefined")

        clientCache = data['clientId'] in self.cache and self.cache[data['clientId']] or { }
        self.cache[data['clientId']] = clientCache
        data['clientId'] = None

        if hasattr(self, "cmd" + cmd):
          getattr(self, "cmd" + cmd)(data, clientCache)
        else:
          self.logerr("Unkown command: %s" % cmd)
      except Exception, e:
        log.err()
      finally:
        self.factory.freeWorker(self.queue)
