#!/bin/bash

ID="service"
PID="$ID.pid"

cd `dirname $0`

function start() {
    if [ -f $PID ] ; then
        echo "$ID: already running"
        return
    fi
    ./index.py -d
    cnt=40
    while [ $cnt -gt 0 ] ; do
        sleep 0.2
        if [ -f $PID ] ; then
            break
        fi
        let cnt=$cnt-1
    done
    if [ $cnt -gt 0 ] ; then
        echo "$ID: started"
    else
        echo "$ID: start failed!"
    fi
}

function stop() {
    if [ ! -f $PID ] ; then
        echo "$ID: not running"
        return
    fi
    kill `cat $PID`
    cnt=40
    while [ $cnt -gt 0 ] ; do
        sleep 0.5
        if [ ! -f $PID ] ; then
            break
        fi
        let cnt=$cnt-1
    done
    if [ $cnt -gt 0 ] ; then
        echo "$ID: stopped"
    else
        echo "$ID: stop failed!"
    fi
}

function status() {
  echo "$ID: status: " `test -f $PID && echo "started" || echo "stopped"`
}

case $1 in
"start")
    start
    ;;
"stop")
    stop
    ;;
"restart")
    stop
    start
    ;;
"status")
    status
    ;;
*)
    echo "usage: $0 {start|stop|restart|status} {log|main|all}"
esac
